---
layout: page
title: "NUCCDC Blog"
---

# The Team's Blog

{% for post in site.posts %}
- **{{ post.date | date: "%B %d, %Y" }}**: [{{ post.title }}]({{ post.url }})
  by _{{ post.author }}_
  - {{ post.tldr }}
{% endfor %}
